import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:baqoel_apps/pages/details_page.dart';
import 'package:baqoel_apps/pages/category_details.dart';
import 'package:baqoel_apps/pages/payment.dart';
import 'package:baqoel_apps/pages/login_page.dart';
import 'package:baqoel_apps/provider/category_provider.dart';
import 'package:provide/provide.dart';


Handler detailsHandler = Handler(
  handlerFunc: (BuildContext context,Map<String,List<String>> params){
    String goodsId = params['id'].first;
    //print('index>details goodsID is ${goodsId}');
    return DetailsPage(goodsId);

  }
);

Handler paymentHandler = Handler(
  handlerFunc: (BuildContext context,Map<String,List<String>> params){
    //String goodsId = params['id'].first;
    //print('index>details goodsID is ${goodsId}');
    return PaymentSuccessPage();

  }
);

Handler categoryHandler = Handler(
  handlerFunc: (BuildContext context, Map<String,List<String>> params){
    // List<String> prm = params['parent'].first.split(':');
    // String parent = prm[0];
    // String sub = prm[1];
    String prm = params['parent'].first;
    Provide.value<CategoryProvider>(context).setId(prm);
    //print('index>details goodsID is ${goodsId}');
    return DetailsCategory();
  }
);

Handler loginHandler = Handler(
  handlerFunc: (BuildContext context,Map<String,List<String>> params){
    //String goodsId = params['id'].first;
    //print('index>details goodsID is ${goodsId}');
    return Login();
  }
);