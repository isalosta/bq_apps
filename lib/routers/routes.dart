import 'package:flutter/material.dart';
import 'package:baqoel_apps/routers/router_handler.dart';
import 'package:fluro/fluro.dart';

class Routes{
  static String root='/';
  static String detailsPage = '/detail';
  static String detailsCategory = '/category';
  static String detailsPayment = '/payment';
  static String login = '/login';

  static void configureRoutes(Router router){
    router.notFoundHandler= new Handler(
      handlerFunc: (BuildContext context,Map<String,List<String>> params){
        print('ERROR====>ROUTE WAS NOT FONUND!!!');
      }
    );

    router.define(detailsPage, handler:detailsHandler);
    router.define(detailsCategory, handler:categoryHandler);
    router.define(detailsPayment, handler:paymentHandler);
    router.define(login, handler:loginHandler);
  }

}