import 'dart:async';

import 'package:baqoel_apps/bloc/detailsinfo_bloc.dart';
import 'package:flutter/material.dart';

import 'package:baqoel_apps/bloc/network_bloc.dart';
import 'package:baqoel_apps/bloc/index_bloc.dart';

import 'package:baqoel_apps/widgets/login.dart';
import 'package:baqoel_apps/widgets/splash.dart';
import 'package:baqoel_apps/widgets/noconnection.dart';
import 'package:baqoel_apps/widgets/index.dart';

import 'package:fluro/fluro.dart';
import 'package:baqoel_apps/routers/routes.dart';
import 'package:baqoel_apps/routers/application.dart';

import 'package:provide/provide.dart';
import 'package:baqoel_apps/provider/cart.dart';
//import 'package:baqoel_apps/provider/category_goods_list.dart';
//import 'package:baqoel_apps/provider/child_category.dart';
import 'package:baqoel_apps/provider/category_provider.dart';
import 'package:baqoel_apps/provider/product_provider.dart';
import 'package:baqoel_apps/provider/details_info.dart';
import 'package:baqoel_apps/provider/credentials.dart';

void main(){
  // var childCategory = ChildCategory();
  // var categoryGoodsListProvide = CategoryGoodsListProvide();
  var productProvide = ProductProvider();
  var categoryProvide = CategoryProvider();
  var detailsInfoProvide= DetailsInfoProvide();
  var cartProvide = CartProvide();
  var providers = Providers();
  var credentialsProvider = CredentialsProvider();

  providers
    // ..provide(Provider<ChildCategory>.value(childCategory))
    // ..provide(Provider<CategoryGoodsListProvide>.value(categoryGoodsListProvide))
    ..provide(Provider<CredentialsProvider>.value(credentialsProvider))
    ..provide(Provider<CategoryProvider>.value(categoryProvide))
    ..provide(Provider<ProductProvider>.value(productProvide))
    ..provide(Provider<CartProvide>.value(cartProvide))
    ..provide(Provider<DetailsInfoProvide>.value(detailsInfoProvide));

  runApp(ProviderNode(child:MyApp(),providers:providers));
}
class MyApp extends StatefulWidget {
  @override
  _MyApp createState() => _MyApp();
}

class _MyApp extends State<MyApp> {

  NetworkStatusBloc _blocNetwork;
  IndexBloc _blocIndex;
  DetailsInfoBloc _blocDetails;

  Map<String, WidgetBuilder> _routes;
  Widget _home;

  _checkNetwork() {
    _blocNetwork.change_status_sink.add(1);

    Future.delayed(const Duration(milliseconds: 5235), () {
      _checkNetwork();
    });
  }

  @override
  void initState() {

    _blocNetwork = NetworkStatusBloc();
    _blocIndex = IndexBloc();
    _blocDetails = DetailsInfoBloc();

    _checkNetwork();
    this.setState(() {
      _routes = <String, WidgetBuilder> {
        "/loginpage": (BuildContext context) => new LoginPage(),
        "/splashpage": (BuildContext context) => new SplashPage(),
        "/networkerror": (BuildContext context) => new NoNetwork(),
        "/home": (BuildContext context) => new IndexPage()
      };
      _home = SplashPage();
    });
  }

  @override
  Widget build(BuildContext context) {
    final router = Router();
    Routes.configureRoutes(router);
    Application.router=router;
    Provide.value<CredentialsProvider>(context).loadToken();
      return MaterialApp(
        title: 'BAQOEL APPS',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: _home,
        onGenerateRoute: Application.router.generator,
        routes: _routes,
      );
    }
}
