import 'package:flutter/material.dart';
import 'package:baqoel_apps/model/details_model.dart';
import 'package:baqoel_apps/sevices.dart';
import 'dart:convert';

class DetailsInfoProvide with ChangeNotifier{
  
   ProductInfo goodsInfo = null;
   bool isLeft = true;
   bool isRight = false;

  getGoodsInfo(String id )async{
    print("GET PRODUCT ID ${id}");

    await getMethod('product/get/' + id).then((val){
      var responseData = json.decode(val.toString());
      print('RES >>>> ${responseData}');
      goodsInfo = ProductInfo.fromJson(responseData['data']);
      notifyListeners();
    });
  }
  changeLeftAndRight(String changeState){
    if(changeState=='left'){
      isLeft=true;
      isRight=false;
    }else{
      isLeft=false;
      isRight=true;
    }
     notifyListeners();

  }


}