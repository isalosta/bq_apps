import 'package:flutter/material.dart';

class CategoryProvider with ChangeNotifier {

  List parents = [];
  List sub = [];

  String ids = "0:0";

  setParents(List data) {
    parents = data;
    ChangeNotifier();
  }

  setSub(List data) {
    print("SAVE SUBS");
    sub = data;
    ChangeNotifier();
  }

  getParents() {
    return parents;
  }

  getSub() {
    return sub;
  }

  setId(String id) {
    print(id);
    ids = id;
    ChangeNotifier();
  }

  getId() {
    return ids;
  }
}