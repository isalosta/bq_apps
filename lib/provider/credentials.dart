import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CredentialsProvider with ChangeNotifier {
  String token = '';
  bool isLogged = false;

  getToken() {
    if(token != '') {
      isLogged = true;
    } else {
      isLogged = false;
    }
    return token;
  }

  saveToken(str) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('credential', str);
    if(str != null || str != '') {
      token = str;
      isLogged = true;
      print("SAVE");
      print(str);
      ChangeNotifier();
    }
  }

  loadToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String s =  pref.getString('credential');
    if(s == null || s == '') {
      return;
    }

    token = s;
      isLogged = true;
      print("Load");
      print(s);
      ChangeNotifier();
  }

  getStatus() {
    return isLogged;
  }
}