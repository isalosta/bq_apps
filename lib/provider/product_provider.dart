import 'package:flutter/material.dart';

class ProductProvider with ChangeNotifier {
  List product = [];

  setProduct(List data) {
    product = data;
    ChangeNotifier();
  }

  getProduct() {
    return product;
  }
}