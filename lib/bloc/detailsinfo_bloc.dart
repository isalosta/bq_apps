import "dart:async";
import 'dart:convert';
import "package:baqoel_apps/model/details_model.dart";
import "package:baqoel_apps/sevices.dart";

class DetailsInfoBloc {
  static DetailsInfoBloc instance;

  StreamController<String> _modelEventController = StreamController<String>();
  StreamSink<String> get change_model_sink => _modelEventController.sink;
  Stream<String> get change_model_stream => _modelEventController.stream;

  StreamController<bool> _posEventController = StreamController<bool>();
  StreamSink<bool> get change_pos_sink => _posEventController.sink;
  Stream<bool> get change_pos_stream => _posEventController.stream;

  StreamController<DetailsModel> _modelStreamController = StreamController<DetailsModel>.broadcast();
  Sink<DetailsModel> get model_sink => _modelStreamController.sink;
  Stream<DetailsModel> get model_stream => _modelStreamController.stream;

  StreamController<bool> _posStreamController = StreamController<bool>();
  StreamSink<bool> get pos_sink => _posStreamController.sink;
  Stream<bool> get pos_stream => _posStreamController.stream;

  DetailsInfoBloc() {
    if(instance == null) {
      instance = this;
      change_model_stream.listen((_) => requesting(_));
      change_pos_stream.listen((_) => pos_sink.add(_));
    }
  }

  requesting(String s){
    var formData = { 'goodId':s, };
    // request('getGoodDetailById',formData:formData).then((val){
    //   var responseData= json.decode(val.toString());
    //   model_sink.add(DetailsModel.fromJson(responseData));
    // });
  }

}