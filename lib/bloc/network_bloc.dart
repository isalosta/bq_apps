import 'dart:async';

class NetworkStatusBloc{

  static NetworkStatusBloc instance;

  StreamController<int> _checkEventController = StreamController<int>();
  StreamSink<int> get change_status_sink => _checkEventController.sink;
  Stream<int> get change_status_stream => _checkEventController.stream;

  StreamController<int> _statusStreamController = StreamController<int>.broadcast();
  Sink<int> get status_sink => _statusStreamController.sink;
  Stream<int> get status_stream => _statusStreamController.stream;

  // final _checkEventController = StreamController<int>.broadcast();
  // Sink<NetworkEvent> get change_status_sink => _checkEventController.sink;

  NetworkStatusBloc() {
    if(instance == null) {
      instance = this;
      _checkEventController.stream.listen((_) => status_sink.add(_));
    }
  }

  @override
  void dispose() {
    _statusStreamController.close();
    _checkEventController.close();
  }

  HandleChange(int value) {
    print('BLOC NET');
    print(value);
    print(status_sink);
    status_sink.add(value);
    //change_status_sink.add(value);
  }

}