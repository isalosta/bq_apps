import 'dart:async';

class IndexBloc{

  static IndexBloc instance;

  StreamController<int> _indexEventController = StreamController<int>();
  StreamSink<int> get change_index_sink => _indexEventController.sink;
  Stream<int> get change_index_stream => _indexEventController.stream;

  StreamController<int> _indexStreamController = StreamController<int>.broadcast();
  Sink<int> get index_sink => _indexStreamController.sink;
  Stream<int> get index_stream => _indexStreamController.stream;

  IndexBloc() {
    if(instance == null) {
      instance = this;
      _indexEventController.stream.listen((_) => index_sink.add(_));
    }
  }

  @override
  void dispose() {
    _indexStreamController.close();
    _indexEventController.close();
  }
}