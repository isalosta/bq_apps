import 'package:flutter/material.dart';
import 'package:provide/provide.dart';
import 'package:baqoel_apps/provider/details_info.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailsTopArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provide<DetailsInfoProvide>(

      builder:(context,child,val){
        var goodsInfo=Provide.value<DetailsInfoProvide>(context).goodsInfo;

        if(goodsInfo != null){
           return Container(
                 color: Colors.white,
                padding: EdgeInsets.all(2.0),
                child: Column(
                  children: <Widget>[
                      _goodsImage( goodsInfo.product_image),
                      _goodsName( goodsInfo.product_name),  
                      _goodsNum(goodsInfo.product_sku),
                      _goodsPrice(goodsInfo.product_price, goodsInfo.product_old_price),
                    
                  ],
                ),
              );

        }else{
          return Text('Loading......');
        }
      }
    );
  }

  Widget _goodsImage(url){
    print("IMAGE URL ${url}");
    return  Image.network(
        url,
        width:ScreenUtil().setWidth(740) 
    );

  }

  Widget _goodsName(name){

      return Container(
        
        width: ScreenUtil().setWidth(730),
        padding: EdgeInsets.only(left:15.0),
        child: Text(
          name,
          maxLines: 1,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(30)
          ),
        ),
      );
  }


  Widget _goodsNum(num){
    return  Container(
     
      width: ScreenUtil().setWidth(730),
      padding: EdgeInsets.only(left:15.0),
      margin: EdgeInsets.only(top:8.0),
      child: Text(
        'SERIAL:${num}',
        style: TextStyle(
          color: Colors.black26
        ),
      ),
      
    );
  }
  Widget _goodsPrice(presentPrice, oriPrice){

    return  Container(
      
      width: ScreenUtil().setWidth(730),
      padding: EdgeInsets.only(left:15.0),
      margin: EdgeInsets.only(top:8.0),
      child: Row(
        children: <Widget>[
          Text(
            'Rp.${presentPrice}',
            style: TextStyle(
              color:Colors.pinkAccent,
              fontSize: ScreenUtil().setSp(40),

            ),

          ),
          Text(
            'Harga: Rp.${oriPrice}',
            style: TextStyle(
              color: Colors.black26,
              decoration: TextDecoration.lineThrough
            ),
                
            
            )
        ],
      ),
    );

  }
}