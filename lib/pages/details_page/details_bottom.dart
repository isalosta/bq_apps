import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provide/provide.dart';
import 'package:baqoel_apps/provider/cart.dart';
import 'package:baqoel_apps/provider/details_info.dart';
import 'package:baqoel_apps/provider/currentIndex.dart';
import 'package:baqoel_apps/provider/credentials.dart';
import 'package:baqoel_apps/routers/application.dart';

class DetailsBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     var goodsInfo=Provide.value<DetailsInfoProvide>(context).goodsInfo;
     
    
    var goodsID= goodsInfo.product_sku;
    var goodsName =goodsInfo.product_name;
    var count = 1;
    var price =goodsInfo.product_price;
    var images= goodsInfo.product_image;

    bool isLogged = Provide.value<CredentialsProvider>(context).getStatus();

    if(!isLogged) {
      return new Container(
       width:ScreenUtil().setWidth(750),
       color: Colors.white,
       height: ScreenUtil().setHeight(80),
       child: InkWell(
                      onTap: (){Application.router.navigateTo(context, '/login');},
                      child: Container(
                        padding: EdgeInsets.all(5.0),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(3.0)
                        ),
                        child: Text(
                          'Login / Sign In',
                          style: TextStyle(
                            color: Colors.white
                          ),
                        ),
                      ),
                    ),
      );
    }
     
    return Container(
       width:ScreenUtil().setWidth(750),
       color: Colors.white,
       height: ScreenUtil().setHeight(80),
       child: Row(
         children: <Widget>[
           Stack(
             children: <Widget>[
               InkWell(
                  onTap: (){
                      Provide.value<CurrentIndexProvide>(context).changeIndex(2);
                      Navigator.pop(context);
                  },
                  child: Container(
                      width: ScreenUtil().setWidth(110) ,
                      alignment: Alignment.center,
                      child:Icon(
                            Icons.shopping_cart,
                            size: 35,
                            color: Colors.red,
                          ), 
                    ) ,
                ),
                Provide<CartProvide>(
                  builder: (context,child,val){
                    int  goodsCount = Provide.value<CartProvide>(context).allGoodsCount;
                    return  Positioned(
                        top:0,
                        right: 10,
                        child: Container(
                          padding:EdgeInsets.fromLTRB(6, 3, 6, 3),
                          decoration: BoxDecoration(
                            color:Colors.pink,
                            border:Border.all(width: 2,color: Colors.white),
                            borderRadius: BorderRadius.circular(12.0)
                          ),
                          child: Text(
                            '${goodsCount}',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(22)
                            ),
                          ),
                        ),
                      ) ;
                  },
                )
              
             ],
           ),
          
           InkWell(
            onTap: ()async {
              await Provide.value<CartProvide>(context).save(goodsID,goodsName,count,price,images);
            },
             child: Container(
               alignment: Alignment.center,
               width: ScreenUtil().setWidth(320),
               height: ScreenUtil().setHeight(80),
               color: Colors.green,
               child: Text(
                 'Add to Shopping Cart',
                 style: TextStyle(color: Colors.white,fontSize: ScreenUtil().setSp(28)),
               ),
             ) ,
           ),
           InkWell(
             onTap: ()async{
                await Provide.value<CartProvide>(context).remove();
                Application.router.navigateTo(context, "/payment");
             },
             child: Container(
               alignment: Alignment.center,
               width: ScreenUtil().setWidth(320),
               height: ScreenUtil().setHeight(80),
               color: Colors.red,
               child: Text(
                 'Buy Now',
                 style: TextStyle(color: Colors.white,fontSize: ScreenUtil().setSp(28)),
               ),
             ) ,
           ),
         ],
       ),
    );
  }
}