import 'package:flutter/material.dart';
import 'package:provide/provide.dart';
import 'package:baqoel_apps/provider/cart.dart';
import './cart_page/cart_item.dart';
import './cart_page/cart_bottom.dart';


class CartPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shopping Cart'),
      ),
      body: FutureBuilder(
        future:_getCartInfo(context),
        builder: (context,snapshot){
          List cartList=Provide.value<CartProvide>(context).cartList;
          print(cartList);
          if(snapshot.hasData && cartList!=null){
              return Stack(
                children: <Widget>[
                  Provide<CartProvide>(
                    builder: (context,child,childCategory){
                       cartList= Provide.value<CartProvide>(context).cartList;
                      //print(cartList);
                      return ListView.builder(
                        itemCount: cartList.length,
                        itemBuilder: (context,index){
                          return CartItem(cartList[index]);
                        },
                      );
                    }
                  ), 
                  Positioned(
                    bottom:0,
                    left:0,
                    child: CartBottom(),
                  )
                ],
              );
        

          }else{
            return Text('Loading');
          }
        },
      ),
    );
  }

  Future<String> _getCartInfo(BuildContext context) async{
     await Provide.value<CartProvide>(context).getCartInfo();
     return 'end';
  }

  
}