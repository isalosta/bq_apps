import 'package:flutter/material.dart';
// import 'package:baqoel_apps/sevices.dart';
// import 'package:baqoel_apps/model/details_model.dart';
// import 'dart:convert';
//import 'package:baqoel_apps/bloc/detailsinfo_bloc.dart';
import 'package:provide/provide.dart';

import 'package:baqoel_apps/provider/details_info.dart';
import 'package:baqoel_apps/pages/details_page/details_top_area.dart';
import 'package:baqoel_apps/pages/details_page/details_explain.dart';
import 'package:baqoel_apps/pages/details_page/details_tabBar.dart';
import 'package:baqoel_apps/pages/details_page/details_web.dart';
import 'package:baqoel_apps/pages/details_page/details_bottom.dart';


class DetailsPage extends StatelessWidget {
  final String goodsId;
  DetailsPage(this.goodsId);
  
   @override
  Widget build(BuildContext context) {
    //print("DETAILS PAGE ${goodsId}");
       return Scaffold(
         appBar: AppBar(
            leading: IconButton(
              icon:Icon(Icons.arrow_back),
              onPressed: (){
                Navigator.pop(context);
              },
              ),
            title: Text('Details Product'),
          ),
          body:FutureBuilder(
            future: _getBackInfo(context),
            builder: (context, snapshot){
              print(snapshot);
              if(snapshot.hasData){
                  return Stack(
                    children: <Widget>[
                      ListView(
                        children: <Widget>[
                            DetailsTopArea(),
                            DetailsExplain(),
                            //DetailsTabBar(),
                            //DetailsWeb(),
                          ],
                        ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        child: DetailsBottom()
                      )
                    ],
                  );
                        
              }else{
                  return Center(
                    heightFactor: 100,
                    widthFactor: 100,
                    child: CircularProgressIndicator(),
                  );
              }
              }
            )
       );
  }

  Future _getBackInfo(BuildContext context ) async {
    
      await Provide.value<DetailsInfoProvide>(context).getGoodsInfo(goodsId);
      return 'ok';
  }

}