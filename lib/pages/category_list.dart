import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:baqoel_apps/provider/category_provider.dart';
import 'dart:convert';
import 'package:baqoel_apps/sevices.dart';
import 'package:provide/provide.dart';
import 'package:baqoel_apps/routers/application.dart';

class CategoryListP extends StatefulWidget {
  _CategoryListP createState() => _CategoryListP();
}

class _CategoryListP extends State<CategoryListP>{
  List<Map> categoryList = [];

  Widget build(BuildContext context) {
    var count = Provide.value<CategoryProvider>(context).getParents().length;

    if(count < 1) {
      return Scaffold(
       appBar: AppBar(title: Text('Categories'),),
       body: FutureBuilder(
         future: getCategory(context),
         builder: (context, snapshot) {
           if(snapshot.data != null) {
             var data = json.decode(snapshot.data.toString());
             List<Map> category = (data['category']['data'] as List).cast();
             List<Map> sub_category = (data['sub_category']['data'] as List).cast();

             Provide.value<CategoryProvider>(context).setParents(category);
             Provide.value<CategoryProvider>(context).setSub(sub_category);

            return CategoryChildList(list: category, sub_list: sub_category,);
           } else {
             return Center(
              child: Text('Please Wait'),
            );
           }
         },
       )
      );
    } else {
      return Provide<CategoryProvider>(
        builder: (context, child, val) {
          List category = val.getParents();
          List subs = val.getSub();

          return CategoryChildList(list: category, sub_list: subs,);
        },
      );
    }
  }
}

class CategoryChildList extends StatelessWidget {
  final List list;
  final List sub_list;
  List<Widget> sub_type;

  CategoryChildList({Key key, this.list, this.sub_list}) : super(key: key);

  Widget _labelUi(BuildContext context, item, index){
    List sub = sub_list.where((f) => f['parent'] == list[index]['ID']).toList();

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox( height: 20,),
          new Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.fromLTRB(20.0, 20.0, 0, 20.0),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width:0.5,color:Colors.white)
              )
            ),
            child:Text(
              item['category_name'],
              style:TextStyle(color:Colors.black, fontSize: 15.0, fontWeight: FontWeight.w100)
              )
          ),
          //Text(item['category_name'], style: TextStyle(fontSize: 14), textAlign: TextAlign.right,),
         // SizedBox( height: 20,),
          _iterationItem(context, sub, index),
        ],
      ),
    );
  }

  Widget _itemUi(BuildContext context, item, index) {
    return InkWell(
      onTap: (){
        Application.router.navigateTo(context, "/category?parent=${item['parent']}:${item['ID']}");
      },
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.all(8.0), 
          child: Image.network(item['link'], width:ScreenUtil().setWidth(70)),
          ),
          Text(item['name'], style: TextStyle(fontSize: 9), )
        ],
      ),
    );
  }

  Widget _iterationItem(BuildContext context, item, idx) {
    var tempIndex=-1;
    return Container(
       //color:Colors.white,
      margin: EdgeInsets.only(top: 5.0),
      height: ScreenUtil().setHeight(item.length > 5 ? 320 : 150),
      padding:EdgeInsets.all(4.0),
      child: GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 5,
        padding: EdgeInsets.all(4.0),
        children: item.map<Widget>((itm){
          //print(itm);
          tempIndex++;
          return _itemUi(context, itm, idx);
        }).toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //int index = -1;
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new Container(
          height: ScreenUtil().setHeight(130),
          child: new ListView.builder(
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              return _labelUi(context, list[index], index);
            },
          ),
        ),
      ],
    );
    // return new Center(
    //     child: new ListView(
    //       children: <Widget>[
    //         new Container(
    //           height: ScreenUtil().setHeight(130),
    //           child: new ListView.builder(
    //             itemCount: list.length,
    //             itemBuilder: (BuildContext context, int index) {
    //               return _gridViewItemUI(context, list[index], index);
    //             },
    //             scrollDirection: Axis.horizontal,
    //           ),
    //         ),
    //       ],
    //     ),
    //   );
  }
}