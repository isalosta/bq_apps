import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:baqoel_apps/provider/category_provider.dart';
import 'package:baqoel_apps/provider/product_provider.dart';
import 'dart:convert';
import 'package:baqoel_apps/sevices.dart';
import 'package:baqoel_apps/routers/application.dart';
import 'package:provide/provide.dart';

class DetailsCategory extends StatefulWidget {
  _DetailsCategory createState() => _DetailsCategory();
}

class _DetailsCategory extends State<DetailsCategory> {
  String parentId = '';
  String subId = '';


 // _DetailsCategory(this.parentId, this.subId);

  Widget _hScroll(BuildContext context, item) {
    return Container(
      height: ScreenUtil().setHeight(250),
      //color: Colors.black,
      child: new ListView.builder(
      itemCount: item.length,
      itemBuilder: (BuildContext context, int index) {
        return new InkWell(
            onTap: () { Provide.value<CategoryProvider>(context).setId("${parentId}:${item[index]['ID']}"); setState(() {
              subId = "${item[index]['ID']}";
            });},
            child: new Container(width: 130.0, 
            color: int.parse(subId) == item[index]['ID'] ? Colors.yellow[100] : null,
              child: new Column(
                children: <Widget>[
                SizedBox(height: 20,),
                new Image.network(item[index]['link']),
                SizedBox(height: 20,),
                new Text(item[index]['name'])
              ],) 
              ),
          ); 
        },
        scrollDirection: Axis.horizontal,),
      );
  }

  Widget _titleWidget(String title){
     return Container(
       alignment: Alignment.centerLeft,
       padding: EdgeInsets.fromLTRB(10.0, 20.0, 0, 20.0),
       decoration: BoxDecoration(
         color:Colors.white,
         border: Border(
           bottom: BorderSide(width:0.5,color:Colors.white)
         )
       ),
       child:Text(
         title,
         style:TextStyle(color:Colors.pink, fontSize: 15.0, fontWeight: FontWeight.bold)
         )
     );
  }

  Widget _wrapList(BuildContext context, List list){

    if(list.length !=0){
       List<Widget> listWidget = list.map((val){
          
          return InkWell(
            onTap:(){
              Application.router.navigateTo(context, "/detail?id=${val['product_sku']}");
            },
            child: 
            Container(
              decoration: BoxDecoration(
                border: new Border.all(
                        color: Colors.black12,
                        width: 0.5,
                        style: BorderStyle.solid
                ),
                borderRadius: BorderRadius.all(Radius.circular(6.0)),
                color: Colors.white
                ),
              width: ScreenUtil().setWidth(350),
              //color:Colors.white,
              padding: EdgeInsets.all(6.0),
              margin:EdgeInsets.only(bottom:3.0, top: 10.0, left: 3.0),
              child: Column(
                children: <Widget>[
                  Image.network(val['product_image'],width: ScreenUtil().setWidth(375),),
                  Text(
                    val['product_name'],
                    maxLines: 1,
                    overflow:TextOverflow.ellipsis ,
                    style: TextStyle(color:Colors.pink,fontSize: ScreenUtil().setSp(26)),
                  ),
                  Row(
                    children: <Widget>[
                      Text('Rp. ${val['product_price']}'),
                      Text(
                        'Rp. ${val['product_old_price']}',
                        style: TextStyle(color:Colors.black26,decoration: TextDecoration.lineThrough),
                        
                      )
                    ],
                  )
                ],
              ), 
            )
           
          );

      }).toList();

      return Wrap(
        spacing: 2,
        children: listWidget,
      );
    }else{
      return Text(' ');
    }
  }

  Widget _productItem(BuildContext context, List list, String title){
    return Container(
          
          child:Column(
            children: <Widget>[
              _titleWidget(title),
               _wrapList(context, list),
            ],
          )   
    );
  }

  Widget build(BuildContext context) {
    List<String> s = Provide.value<CategoryProvider>(context).getId().split(':');
    if(parentId == '' && subId == '') {
      parentId = s[0];
      subId = s[1];
    }

    List sub = Provide.value<CategoryProvider>(context).getSub().where((v) => v['parent'] == int.parse(parentId)).toList();
    List product = Provide.value<ProductProvider>(context).getProduct().where((v) => subId != '0' ? v['product_category'] == parentId && v['product_subcategory'] == subId : v['product_category'] == parentId).toList();
    String name = subId == '0' || parentId == '10' ? sub[0]['name'] : sub.where((v) => v['ID'] == int.parse(subId)).toList()[0]['name'];

    name = "${name} (${product.length} Products)";
    return Scaffold(
      appBar: AppBar(
            leading: IconButton(
              icon:Icon(Icons.arrow_back),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
          title: Text('Category Details'),
          ),
      body: new Container(
        child: ListView(
          children: <Widget>[
            _hScroll(context, sub),
            _productItem(context, product, name)
            //ItemBuilder(itemList: product)
          ],
        ),
      ),
    );
  }
}

class ItemBuilder extends StatelessWidget {
  final List itemList;

  ItemBuilder({Key key, this.itemList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            _titleWidget(),
            _itemBuilder(context)
          ],
        ),
      );
   
  }

  Widget _titleWidget(){
     return Container(
       alignment: Alignment.centerLeft,
       padding: EdgeInsets.fromLTRB(10.0, 20.0, 0, 20.0),
       decoration: BoxDecoration(
         color:Colors.white,
         border: Border(
           bottom: BorderSide(width:0.5,color:Colors.white)
         )
       ),
       child:Text(
         'Products Featured',
         style:TextStyle(color:Colors.pink, fontSize: 15.0, fontWeight: FontWeight.bold)
         )
     );
  }

  Widget _itemBuilder(BuildContext context){

      return  Container(
        height: ScreenUtil().setHeight(380),
       
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: itemList.length,
          itemBuilder: (context,index){
            return _item(index,context);
          },
        ),
      );
  }

  Widget _item(index,context){
    return InkWell(
      onTap: (){
         Application.router.navigateTo(context, "/detail?id=${itemList[index]['product_sku']}");
      },
      child: Container(
        width: ScreenUtil().setWidth(280),
        padding: EdgeInsets.all(10.0),
        decoration:BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(6.0)),
          // color:Colors.white,
          // border:Border(
          //   left: BorderSide(width:0.5,color:Colors.black12)
          // )
        ),
        child:Column(
          children: <Widget>[
            //Padding(padding: EdgeInsets.all(8.0),),
            Image.network(itemList[index]['product_image']),
            Text('Rp. ${itemList[index]['product_price']}'),
            Text(
              'Rp. ${itemList[index]['product_old_price']}',
              style: TextStyle(
                decoration: TextDecoration.lineThrough,
                color:Colors.grey
              ),
            )
          ],
        ),
      ),
    );
  }
}