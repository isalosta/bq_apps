import 'package:flutter/material.dart';
import 'package:baqoel_apps/sevices.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'dart:convert';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:baqoel_apps/routers/application.dart';
import 'package:baqoel_apps/provider/product_provider.dart';
import 'package:provide/provide.dart';
import 'package:baqoel_apps/bloc/index_bloc.dart';

//import 'package:baqoel_apps/model/category_model.dart';


class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> with AutomaticKeepAliveClientMixin {

  int page = 1;
  List<Map> hotGoodsList=[];
  
  @override
  bool get wantKeepAlive =>true;

  @override
  void initState() {
    super.initState();
     
  }

  GlobalKey<EasyRefreshState> _easyRefreshKey =new GlobalKey<EasyRefreshState>();
  GlobalKey<RefreshFooterState> _footerKey = new GlobalKey<RefreshFooterState>();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return  Scaffold(
      //backgroundColor: Color.fromRGBO(244, 245, 245, 1.0),
      appBar: AppBar(title: Text('Baqoel'),),
      body:FutureBuilder(
        future: getHome(context), //request('homePageContext',formData:formData),
        builder: (context, snapshot){
          if(snapshot.data != null) {
            var data = json.decode(snapshot.data.toString());

            List<Map> productList = (data['product']['data'] as List).cast();
            List<Map> category = (data['category']['data'] as List).cast();
            List<Map> banner = (data['banner']['data'] as List).cast();

            List<Map> recommendList = new List();
            List<Map> featuredList = new List();

            for(int i = 0; i < productList.length; i++) {
              if(productList[i]['product_recommended']) {
                 recommendList.add(productList[i]);
              }
              if(productList[i]['product_featured']) {
                featuredList.add(productList[i]);
              }
            }

            Provide.value<ProductProvider>(context).setProduct(productList);

            return EasyRefresh (
                refreshFooter: ClassicsFooter(
                  key:_footerKey,
                  bgColor:Colors.white,
                  textColor: Colors.pink,
                  moreInfoColor: Colors.pink,
                  showMore: true,
                  noMoreText: '',
                  //moreInfo: 'More Info',
                  //loadReadyText:'Loading...'

                ),
                child: ListView(
                  children: <Widget>[
                    SwiperDiy(swiperDataList: banner),
                    TopNavigator(navigatorList: category),
                    //   //SwiperDiy(swiperDataList: swiperDataList),
                    //  // SwiperDiy(swiperDataList: swiperDataList),
                    // //  AdBanner(advertesPicture:advertesPicture), 
                    // //  LeaderPhone(leaderImage:leaderImage,leaderPhone: leaderPhone),
                    Recommend(recommendList:recommendList),    
                       //FloorTitle(picture_address:floor1Title),
                       //FloorContent(floorGoodsList:floor1),
                    //   FloorTitle(picture_address:floor2Title),
                    //   FloorContent(floorGoodsList:floor2),
                    //   FloorTitle(picture_address:floor3Title),
                    //   FloorContent(floorGoodsList:floor3),
                     _hotGoods(featuredList),
                      
                    ],
              ) ,
              loadMore: ()async{
                  //var formPage={'page': page};
                  // await  request('homePageBelowConten',formData:formPage).then((val){
                  //   var data=json.decode(val.toString());
                  //   List<Map> newGoodsList = (data['data'] as List ).cast();
                  //   setState(() {
                  //     hotGoodsList.addAll(newGoodsList);
                  //     page++; 
                  //   });
                  // });
                },
            );
           
          }else{
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      )
    );

  }

  void _getHotGoods(){
    //  var formPage={'page': page};
    //  request('homePageBelowConten',formData:formPage).then((val){
       
    //    var data=json.decode(val.toString());
    //    List<Map> newGoodsList = (data['data'] as List ).cast();
    //    setState(() {
    //      hotGoodsList.addAll(newGoodsList);
    //      page++; 
    //    });
       
     
    //  });
  }

  Widget hotTitle= Container(
        margin: EdgeInsets.only(top: 10.0),
        padding: EdgeInsets.all(10.0),
        alignment:Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          border:Border(
            bottom: BorderSide(width:0.5 ,color:Colors.white)
          )
        ),
        child: 
        Padding(padding: EdgeInsets.all(8.0), child: Text('Hot Items', style: TextStyle(fontWeight: FontWeight.bold))),
   );

  Widget _wrapList(List list){

    if(list.length!=0){
       List<Widget> listWidget = list.map((val){
          
          return InkWell(
            onTap:(){
              Application.router.navigateTo(context,"/detail?id=${val['product_sku']}");
            },
            child: 
            Container(
              decoration: BoxDecoration(
                border: new Border.all(
                        color: Colors.black12,
                        width: 0.5,
                        style: BorderStyle.solid
                ),
                borderRadius: BorderRadius.all(Radius.circular(6.0)),
                color: Colors.white
                ),
              width: ScreenUtil().setWidth(350),
              //color:Colors.white,
              padding: EdgeInsets.all(6.0),
              margin:EdgeInsets.only(bottom:3.0, top: 10.0, left: 3.0),
              child: Column(
                children: <Widget>[
                  Image.network(val['product_image'],width: ScreenUtil().setWidth(375),),
                  Text(
                    val['product_name'],
                    maxLines: 1,
                    overflow:TextOverflow.ellipsis ,
                    style: TextStyle(color:Colors.pink,fontSize: ScreenUtil().setSp(26)),
                  ),
                  Row(
                    children: <Widget>[
                      Text('Rp. ${val['product_price']}'),
                      Text(
                        'Rp. ${val['product_old_price']}',
                        style: TextStyle(color:Colors.black26,decoration: TextDecoration.lineThrough),
                        
                      )
                    ],
                  )
                ],
              ), 
            )
           
          );

      }).toList();

      return Wrap(
        spacing: 2,
        children: listWidget,
      );
    }else{
      return Text(' ');
    }
  }

  Widget _hotGoods(List list){
    return Container(
          
          child:Column(
            children: <Widget>[
              hotTitle,
               _wrapList(list),
            ],
          )   
    );
  }
}


class SwiperDiy extends StatelessWidget{

  final List swiperDataList;
  SwiperDiy({Key key, this.swiperDataList}):super(key:key);

  @override
  Widget build(BuildContext context) {
     print("RENDER");

     if(swiperDataList.length > 0)
     //print("${swiperDataList[0]['image']}");

    // return new Container();
    return new Container(
      color:Colors.white,
      height: ScreenUtil().setHeight(333),
      width: ScreenUtil().setWidth(750),
      margin: EdgeInsets.only(bottom: 5.0),
      child: Swiper(
        itemBuilder: (BuildContext context, int index){
          return InkWell(
            onTap: (){
               Application.router.navigateTo(context,"/detail?id=${swiperDataList[index]['banner_code']}");

            },
            child:  Image.network("${swiperDataList[index]['banner_link']}",fit:BoxFit.fill),
          );
         
        },
        itemCount: swiperDataList.length,
        pagination: new SwiperPagination(),
        autoplay: true,
      ),
    );
  }
}


class TopNavigator extends StatelessWidget {
  final List navigatorList;
  final IndexBloc _bloc = IndexBloc.instance;

  TopNavigator({Key key, this.navigatorList}) : super(key: key);
  Widget _gridViewItemUI(BuildContext context,item,index){
    return InkWell(
      onTap: (){
        item['ID'] == 10 ? _bloc.change_index_sink.add(1) : Application.router.navigateTo(context,"/category?parent=${item['ID']}:0");
        //_goCategory(context, index, item['id']);
      },
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.all(8.0), 
          child: Image.network(item['category_link'], width:ScreenUtil().setWidth(70)),
          ),
          Text(item['category_name'], style: TextStyle(fontSize: 9), )
        ],
      ),
    );
  }

  void _goCategory(context,int index, String categroyId) async {
    // await request('getCategory').then((val) {
    //   var data = json.decode(val.toString());
    //   CategoryModel category = CategoryModel.fromJson(data);
    //   List   list = category.data;
    //   // Provide.value<ChildCategory>(context).changeCategory(categroyId,index);
    //   // Provide.value<ChildCategory>(context).getChildCategory( list[index].bxMallSubDto,categroyId);
    //   // Provide.value<CurrentIndexProvide>(context).changeIndex(1);
    // });
  }

  @override
  Widget build(BuildContext context) {

    if(navigatorList.length>10){
      navigatorList.removeRange(10, navigatorList.length);
    }
    var tempIndex=-1;
    return Container(
      //color:Colors.white,
      margin: EdgeInsets.only(top: 5.0),
      height: ScreenUtil().setHeight(320),
      padding:EdgeInsets.all(4.0),
      child: GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 5,
        padding: EdgeInsets.all(4.0),
        children: navigatorList.map((item){
          tempIndex++;
          return _gridViewItemUI(context, item,tempIndex);
          
        }).toList(),
      ),
    );
  }
}


class AdBanner extends StatelessWidget {
  final String advertesPicture;

  AdBanner({Key key, this.advertesPicture}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   

    return Container(
      margin: EdgeInsets.only(top:5.0),
      color:Colors.white,
      child: Image.network(advertesPicture),
    );
  }
}

class LeaderPhone extends StatelessWidget {
  final String leaderImage;
  final String leaderPhone;

  LeaderPhone({Key key, this.leaderImage,this.leaderPhone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap:_launchURL,
        child: Image.network(leaderImage),
      ),
    );
  }

  void _launchURL() async {
    String url = 'tel:'+leaderPhone;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}


class Recommend extends StatelessWidget {
  final List  recommendList;

  Recommend({Key key, this.recommendList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            _titleWidget(),
            _recommedList(context)
          ],
        ),
      );
   
  }

  Widget _titleWidget(){
     return Container(
       alignment: Alignment.centerLeft,
       padding: EdgeInsets.fromLTRB(10.0, 20.0, 0, 20.0),
       decoration: BoxDecoration(
         color:Colors.white,
         border: Border(
           bottom: BorderSide(width:0.5,color:Colors.white)
         )
       ),
       child:Text(
         'Products Featured',
         style:TextStyle(color:Colors.pink, fontSize: 15.0, fontWeight: FontWeight.bold)
         )
     );
  }

  Widget _recommedList(BuildContext context){

      return  Container(
        height: ScreenUtil().setHeight(380),
       
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: recommendList.length,
          itemBuilder: (context,index){
            return _item(index,context);
          },
        ),
      );
  }

  Widget _item(index,context){
    return InkWell(
      onTap: (){
         Application.router.navigateTo(context,"/detail?id=${recommendList[index]['product_sku']}");
      },
      child: Container(
        width: ScreenUtil().setWidth(280),
        padding: EdgeInsets.all(10.0),
        decoration:BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(6.0)),
          // color:Colors.white,
          // border:Border(
          //   left: BorderSide(width:0.5,color:Colors.black12)
          // )
        ),
        child:Column(
          children: <Widget>[
            //Padding(padding: EdgeInsets.all(8.0),),
            Image.network(recommendList[index]['product_image']),
            Text('Rp. ${recommendList[index]['product_price']}'),
            Text(
              'Rp. ${recommendList[index]['product_old_price']}',
              style: TextStyle(
                decoration: TextDecoration.lineThrough,
                color:Colors.grey
              ),
            )
          ],
        ),
      ),
    );
  }
}



class FloorTitle extends StatelessWidget {
  final String picture_address;
  FloorTitle({Key key, this.picture_address}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Image.network(picture_address),
    );
  }
}

class FloorContent extends StatelessWidget {
  final List floorGoodsList;

  FloorContent({Key key, this.floorGoodsList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _firstRow(context),
          _otherGoods(context)
        ],
      ),
    );
  }

  Widget _firstRow(context){
    return Row(
      children: <Widget>[
        _goodsItem(context,floorGoodsList[0]),
        Column(
          children: <Widget>[
           _goodsItem(context,floorGoodsList[1]),
           _goodsItem(context,floorGoodsList[2]),
          ],
        )
      ],
    );
  }

  Widget _otherGoods(context){
    return Row(
      children: <Widget>[
       _goodsItem(context,floorGoodsList[3]),
       _goodsItem(context,floorGoodsList[4]),
      ],
    );
  }

  Widget _goodsItem(context,Map goods){

    return Container(
      width:ScreenUtil().setWidth(375),
      child: InkWell(
        onTap:(){
          Application.router.navigateTo(context, "/detail?id=${goods['product_sku']}");
        },
        child: Image.network(goods['product_image']),
      ),
    );
  }

}



  
