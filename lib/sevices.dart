import "package:dio/dio.dart";
import 'dart:async';
import 'dart:io';
import 'package:baqoel_apps/config/url.dart';
import 'dart:convert';

import 'package:flutter/widgets.dart';

Future getMethod(String url) async {
  String uri = base+url;
  print(uri);
  
  try {
    Response res;
    Dio dio = new Dio();
    res = await dio.get(uri);

    return res;

  } catch(e) {
    return print("Error Request ${e}");
  }
}

Future<String> getHome(context) async {
  String uri = base;
  //print(uri);

  try {
    Response product;
    Response category;
    Response banner;

    Dio dio = new Dio();
    product = await dio.get(uri+"product/get");
    category = await dio.get(uri+"category/get");
    banner = await dio.get(uri+"banner/get");

    Map<String, dynamic> data={
      'product': json.decode(product.toString()),
      'category': json.decode(category.toString()),
      'banner': json.decode(banner.toString())
    };
    return json.encode(data);
  } catch(err) {
    Navigator.of(context).pushReplacementNamed("/networkerror");
    //return err.toString();
  }
}


Future<String> getCategory(context) async {
  String uri = base;

  try {
    Response category;
    Response sub_category;

    Dio dio = new Dio();
    category = await dio.get(uri + 'category/get');
    sub_category = await dio.get(uri + 'subcategory/get');

    Map<String, dynamic> data = {
      'category': json.decode(category.toString()),
      'sub_category' : json.decode(sub_category.toString())
    };

    return json.encode(data);

  } catch(err) {
    Navigator.of(context).pushReplacementNamed("/networkerror");
  }
}

Future<Map> loginRequest(context, dataX) async {
  String uri = base;

  try {
    Response lgn;
    Dio dio = new Dio();
    dio.options.headers = {
      "Content-Type" : "application/json"
    };
    print(json.encode(dataX));
    lgn = await dio.post(uri + 'user/login', data: json.encode(dataX));
    print(json.decode(lgn.toString()));
    Map<String, dynamic> data = {
      'user': json.decode(lgn.toString())
    };
    return data;
  } catch(e) {
    Map<String, dynamic> data = {
      'report': e.toString()
    };
    return data;
  }

}


