import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:baqoel_apps/pages/home.dart';
import 'package:baqoel_apps/pages/cart_page.dart';
import 'package:baqoel_apps/pages/profile_page.dart';
import 'package:baqoel_apps/pages/category_list.dart';
import 'package:baqoel_apps/bloc/index_bloc.dart';
import 'package:baqoel_apps/widgets/noconnection.dart';

class IndexPage extends StatelessWidget {
  final IndexBloc _bloc = IndexBloc.instance;
  final List<BottomNavigationBarItem> bottomTabs = [
    BottomNavigationBarItem(
      icon:Icon(CupertinoIcons.home),
      title:Text('Home')
    ),
    BottomNavigationBarItem(
      icon:Icon(CupertinoIcons.search),
      title:Text('Category')
    ),
    BottomNavigationBarItem(
      icon:Icon(CupertinoIcons.shopping_cart),
      title:Text('Cart')
    ),
     BottomNavigationBarItem(
      icon:Icon(CupertinoIcons.profile_circled),
      title:Text('Profile')
    ),
  ];

   final List<Widget> tabBodies = [
      HomePage(),
      CategoryListP(), //CategoryPage(),
      CartPage(),
      ProfilePage()
   ];

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return new StreamBuilder(
      stream: _bloc.index_stream,
      initialData: 0,
      builder: (context, snapshot){
        return Scaffold(
            backgroundColor: Color.fromRGBO(244, 245, 245, 1.0),
            bottomNavigationBar: BottomNavigationBar(
              type:BottomNavigationBarType.fixed,
              currentIndex: snapshot.data,
              items:bottomTabs,
              onTap: (index){
                _bloc.change_index_sink.add(index);
              },
            ),
             body: IndexedStack(
                    index: snapshot.data,
                    children: tabBodies
                  ),
        ); 
      }
    );
     
  }
}