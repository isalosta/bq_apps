import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  int _status;
  String _token;

  Future<String> _readPref() async{
    Future<SharedPreferences> _pref = SharedPreferences.getInstance();
    _pref.then((pref){
      final key = "TOKEN_BAQOEL_APPS";
      final value = pref.getString(key) ?? "";
      this.setState((){
        _token = value;
      });
      return value;
    }).catchError((err){
      return null;
    });
  }

  @override
  void initState() {
    super.initState();
    print("INITIALIZE");
    Future.delayed(const Duration(milliseconds: 2000), (){
      this.setState((){
        _status = 1;
        });

        _readPref().then((val) {
          Navigator.of(context).pushReplacementNamed("/home");
        });
      
    });
  }

  @override
  Widget build(BuildContext context) {
    if(_status == 1) {
     // print(sprintf("Status %i", [_status, _token]));
      return new MaterialApp(
        title: 'Flutter Demo',
        home: new Scaffold(
          appBar: null,// new AppBar(
          //   title: new Text("Splash Screen Example"),
          // ),
          body: new Center(
            child: new Text("Finalizing Baqoel"),
          ),
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Center(
        child: new Image(
            width: 250.0,
            height: 191.0,
            fit: BoxFit.scaleDown,
            image: new AssetImage('images/logo.png'),
        ),
      ),
    );
  }
}
